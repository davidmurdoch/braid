/**
 * Copyright 2018 Royal Bank of Scotland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.bluebank.braid.corda.rest.docs

import io.bluebank.braid.corda.rest.HTTP_UNPROCESSABLE_STATUS_CODE
import io.bluebank.braid.corda.rest.nonEmptyOrNull
import io.bluebank.braid.core.annotation.MethodDescription
import io.swagger.annotations.ApiOperation
import io.swagger.models.Model
import io.swagger.models.Operation
import io.swagger.models.Response
import io.swagger.models.parameters.BodyParameter
import io.swagger.models.parameters.Parameter
import io.swagger.models.parameters.PathParameter
import io.swagger.models.parameters.QueryParameter
import io.swagger.models.properties.ArrayProperty
import io.swagger.models.properties.Property
import io.swagger.models.properties.PropertyBuilder
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpMethod.*
import io.vertx.ext.web.RoutingContext
import java.lang.reflect.Type
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response.Status.*
import kotlin.reflect.KParameter
import kotlin.reflect.KType

abstract class EndPoint(
  private val groupName: String,
  val protected: Boolean,
  val method: HttpMethod,
  val path: String,
  val modelContext: ModelContext
) {
  companion object {
    fun create(
      groupName: String,
      protected: Boolean,
      method: HttpMethod,
      path: String,
      name: String,
      parameters: List<KParameter>,
      returnType: KType,
      annotations: List<Annotation>,
      modelContext: ModelContext
    ): EndPoint {
      return KEndPoint(
        groupName,
        protected,
        method,
        path,
        name,
        parameters,
        returnType.javaTypeIncludingSynthetics(),
        annotations,
        modelContext
      )
    }

    fun create(
      groupName: String,
      protected: Boolean,
      method: HttpMethod,
      path: String,
      modelContext: ModelContext,
      fn: RoutingContext.() -> Unit
    ): EndPoint {
      return ImplicitParamsEndPoint(groupName, protected, method, path, modelContext, fn)
    }
  }

  abstract val returnType: Type
  protected abstract val annotations: List<Annotation>
  abstract val parameterTypes: List<Type>

  private val apiOperation: ApiOperation? by lazy {
    annotations.filterIsInstance<ApiOperation>().firstOrNull()
  }

  private val methodDescription: MethodDescription? by lazy {
    annotations.filterIsInstance<MethodDescription>().firstOrNull()
  }

  val responseContainer: String?
    get() {
      return apiOperation?.responseContainer
    }

  val description: String
    get() {
      return methodDescription?.description?.nonEmptyOrNull()
        ?: apiOperation?.value?.nonEmptyOrNull()
        ?: ""
    }

  open val produces: String
    get() {
      return if (apiOperation != null && !apiOperation!!.produces.isBlank()) {
        apiOperation!!.produces
      } else {
        returnType.mediaType()
      }
    }

  open val consumes: String
    get() {
      return if (apiOperation != null && !apiOperation!!.consumes.isBlank()) {
        apiOperation!!.consumes
      } else {
        mapBodyParameter()?.schema?.properties?.keys?.first() ?: returnType.mediaType()
      }
    }

  fun addTypes(models: ModelContext) {
    models.addType(this.returnType)
    this.parameterTypes.forEach {
      models.addType(it)
    }
  }

  fun toOperation(): Operation {
    val operation = Operation().consumes(consumes)
    operation.description = description
    decorateOperationWithResponseType(operation)
    operation.parameters = toSwaggerParams()
    operation.tag(groupName)
    if (protected) {
      operation.addSecurity(DocsHandlerV2.SECURITY_DEFINITION_NAME, listOf())
    }
    operation.addResponse(OK.statusCode.toString(), operation.responses["default"])
    operation.addResponse(
      INTERNAL_SERVER_ERROR.statusCode.toString(),
      Response().description("server failure").responseSchema(Throwable::class.java.getSwaggerModelReference())
    )
    operation.addResponse(
      BAD_REQUEST.statusCode.toString(),
      Response().description("the server failed to parse the request").responseSchema(Throwable::class.java.getSwaggerModelReference())
    )
    operation.addResponse(
      HTTP_UNPROCESSABLE_STATUS_CODE.toString(),
      Response().description("the request could not be processed").responseSchema(Throwable::class.java.getSwaggerModelReference())
    )
    return operation
  }

  protected abstract fun mapBodyParameter(): BodyParameter?
  protected abstract fun mapQueryParameters(): List<QueryParameter>
  protected abstract fun mapPathParameters(): List<PathParameter>

  protected open fun toSwaggerParams(): List<Parameter> {
    return when (method) {
      GET, HEAD, DELETE, CONNECT, OPTIONS -> {
        val pathParams = mapPathParameters()
        val queryParams = mapQueryParameters()
        return pathParams + queryParams
      }
      else -> {
        val pathParameters = mapPathParameters() as List<Parameter>
        val queryParams = mapQueryParameters()
        val bodyParameter = mapBodyParameter()
        if (bodyParameter != null) {
          pathParameters + queryParams + bodyParameter
        } else {
          pathParameters + queryParams
        }
      }
    }
  }

  protected fun KType.getSwaggerProperty(): Property {
    return getKType().javaTypeIncludingSynthetics().getSwaggerProperty()
  }

  protected fun KType.getSwaggerModelReference(): Model {
    val property = getSwaggerProperty()
    return PropertyBuilder.toModel(property)
  }

  protected fun Type.getSwaggerModelReference(): Model {
    val property = getSwaggerProperty()
    return PropertyBuilder.toModel(property)
  }

  protected fun Type.getSwaggerProperty(): Property {
    try {
      return modelContext.getProperty(this)
    } catch (e: Throwable) {
      throw RuntimeException("Unable to convert actual type: $this", e)
    }
  }

  private fun decorateOperationWithResponseType(operation: Operation) {
    if (returnType.isEmptyResponseType()) {
      operation
        .produces(MediaType.TEXT_PLAIN)
        .defaultResponse(Response().description("empty response"))
    } else {
      val responseSchema = returnType.getSwaggerProperty().let { responseSchema ->
        when (responseContainer) {
          "List", "Array", "Set" -> {
            ArrayProperty(responseSchema)
          }
          else -> {
            responseSchema
          }
        }
      }
      @Suppress("DEPRECATION")
      operation
        .produces(produces)
        .defaultResponse(Response().schema(responseSchema).description("default response"))
    }
  }
}
