/**
 * Copyright 2018 Royal Bank of Scotland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.bluebank.braid.core.utils

import java.io.File
import java.net.URI
import java.net.URISyntaxException
import java.net.URL
import java.net.URLClassLoader
import java.util.Arrays.asList

object PathsClassLoader {
  private val tempFileDownloader = JarDownloader()

  fun jarsClassLoader(vararg jarPaths: String) =
    jarsClassLoader(jarPaths.toList())

  fun jarsClassLoader(jarPaths: Collection<String>): ClassLoader {
    return when {
      jarPaths.isEmpty() -> Thread.currentThread().contextClassLoader
      else -> {
        val urls = jarPaths.asSequence().map {
          urlOrFiles(it)
        }.flatMap { it.asSequence() }.toList().toTypedArray()
        URLClassLoader(urls, Thread.currentThread().contextClassLoader)
      }
    }
  }

  fun urlOrFiles(urlOrFileName: String): List<URL> {
    return try {
      // attempt to download the file if available
      asList(tempFileDownloader.uriToFile(URI(urlOrFileName).toURL()))
    } catch (err: IllegalArgumentException) {
      localFiles(File(urlOrFileName))
    } catch (err: URISyntaxException) {
      // URI throws this exception if urlOrFileName starts with like "C:\" on Windows
      localFiles(File(urlOrFileName))
    }
  }

 private fun localFiles(file: File): List<URL> {
      // attempt to create as a path
      if(file.isDirectory){
        val list = file.listFiles()
        val toList = list.map { localFiles(it) }.flatMap { it }.toList()
        return toList
      }
      return asList(file.toURI().toURL())
    }
}

fun List<String>.toJarsClassLoader(): ClassLoader {
  return PathsClassLoader.jarsClassLoader(this)
}
